# Intro to Intro to Programming

Stuff that should have been touched oin the first week of CS4141 (in my opinion).  
There are many tools and services that devs use, all of them make things more intutive and easier.


[[_TOC_]]


# Git
Git is the most common form of source control in use today.  
There are a few others in use, namely SVN and Mercurial but those are the minorty.

Git itself is a command line utility, but it interfaces with most IDE's as well as most automation systems related to code.

## Get git
Download from [here][git] and install it.

[Here is a decent interactive tutorial][git-learn] but I have the basics below.

[git]: https://git-scm.com/downloads
[git-learn]: https://learngitbranching.js.org/

## Basic use

```bash
# start new repo in teh current folder
git init


# Check teh status of the repository, this tells you what files are new (to git) and which oens have been modified.
# it also goes over the next two commands
git status


# let git know you want to track changes in a specific file
git add example.txt

# track all files
git add *

# track all .txt files
git add *.txt


# commit (save) the files that have been added, along with a message for teh commit
git commit -m "message"


# switch to a different branch.
# branches are where you can do work without fear of fecking up your previous code.
git branch -b newBranch


# add a remote repo
git remote add origin https://gitlab.com/example/project.git


# push your changes to the main branch to teh remote repo
git push origin main


# there are changes on teh remopte repo, you want to grab them
git pull origin main
```

I know there are a fair few commands tehre but the main ones are add, commit, push and pull.  
For more advanced stuff just google ``Git how to ___``

## Online Repos
When most folks think of git they think of Github, Github is not Git, it uses git to provide and online repository that makes it easy to share and collaborate.

I personally prefer gitlab (you are now on it).  
You can sign into gitlab with a github account (via oauth2)

* [Gitlab]
* [Github]
  * [Has a student bundle that you may find useful][Github-edu]
* [Bitbucket]


[Gitlab]: https://about.gitlab.com/get-started/
[Github]: https://github.com/join
[Github-edu]: https://education.github.com/pack
[Bitbucket]: https://bitbucket.org/product

# IDE's
Intergrated Delevopment Enviroment.  

Bluejay is a step in teh right direction but it is nowhere near as good as it can be.

The two main ones are VSCode and Intellij.  

## VS Code
Microsoft have created this, it is Open Source and available for Windows, Mac and Linux.  
Its free to download and use and has many plugins that make life easier.

[VSCode]


## Intellij
Jetbrains is the company who has made Intellij, they have different IDE's depending on teh language you are using, for example for the Java module I am using IDEA Ultimate.

Their products are probally the best ye can get, they do cost money to buy normally, however they are free to students.   
If you are going this route (which I recommend if you are on windows) I would also getting teh toolbox to manage the sub-ide's.

[Intellij]  
[Intellij-toolbox]

[VSCode]: https://code.visualstudio.com/
[Intellij]: https://www.jetbrains.com/community/education/#students
[Intellij-toolbox]: https://www.jetbrains.com/toolbox-app/


# Communication

We kinda got communication covered with teh discord, but it is one of the most important tools to have.  

# Practical problems to solve
My leap ahead mentor said it would be a good idea to learn C over the summer as one of the lecturers will require us to use it, without teaching us about it.

I am not sure about you but I learn code related stuff best when I have a goal/problem to solve.  

Thankfully there is a site with good challenges that build on top of each toehr.

## Advent of Code (AoC)
[Advent of Code][AoC] is a yearly event (during Advent) where there are 25 problems to solve, with one being released for each day of advent.

You are free to use any language ye want or even to do it in multiple languages (heck you could even do it by hand)  
There is no need to wait till advent to do it, there is an entire [back catogoluge][AoC-Past] that you can tackle.

[AoC]: https://adventofcode.com/
[AoC-Past]: https://adventofcode.com/2021/events
